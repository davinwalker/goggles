# Goggles

[SOS Report](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos)

SOS Report / Log Parser

The goal is to create a way to easily consume sos report data
- Parse / Index Logs into ELK
    - Prebuilt Graphs/Statistics
- Quick Searching / Navigating
    - Host and Category Filtering / Text Search
- Present simple views of common diagnostic info. 
    - Host resources. CPU/Memory/Disk
    - Services / Gitlab Version / Listening Network

Future Goals
- Identify Common or Known Issues


## Usage

```
docker-compose up --build

# Purge data (ELK) and spin back up
docker-compose down && docker-compose up --build
```

1. Spin up environment `docker-compose up`
2. Navigate to http://localhost:4567
3. Upload SOS Report

![overview](/overview.gif)

## Known Issues

- [ ] Startup Time is deceptive (You can upload a log before ELK is ready)

## Misc

#### Quickly Generate New SOS Report

```
curl https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/raw/master/gitlabsos.rb | /opt/gitlab/embedded/bin/ruby
```

### Export Dashboard

```
Kibana.export_dashboard
```

```
curl -X GET "http://localhost:5601/api/kibana/dashboards/export?dashboard=942dcef0-b2cd-11e8-ad8e-85441f0c2e5c" -H 'kbn-xsrf: true'

curl -X GET "http://localhost:5601/api/saved_objects/_find?type=dashboard&search=display" -H 'kbn-xsrf: true'
```



# Kibana

Query Examples

- Field Exists
  ` _exists_: username` 


