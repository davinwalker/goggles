# System Helper
module System
  @list = []
  @loading = 0
  @current = nil
  @mutex = Mutex.new

  def self.bump(count = 1)
    @loading += count
  end

  def self.done(count = 1)
    @loading -= count
  end

  def self.clear
    @list.clear
  end

  def self.percent
    if @loading.zero?
      0
    else
      100 - @loading * 2
    end
  end

  def self.build_path(path, dir)
    tmp_path = path.gsub(dir + '/', '')

    case tmp_path.count('/')
    when 0
      tmp_path
    when 1
      tmp_path.split('/').last
    else
      tmp_path.split('/').last(2).join('/')
    end
  end

  def self.current(file = nil)
    @current = file unless file.nil?
    @current
  end

  def self.loading
    @loading
  end

  def self.all
    @list
  end

  def self.add(sys)
    @list.push sys
  end

  def self.list
    @list
  end

  def self.upload(file)
    @mutex.lock
    bump
    name = File.basename file['filename'], '.tar.bz2'
    path = file['tempfile'].path

    load(name, path)
    done
  ensure
    @current = nil
    @mutex.unlock
  end

  def self.load(name, path)
    sys = Host.new(name)
    Dir.mktmpdir('sauce-upload') do |dir|
      `bsdtar -xf #{path} -C #{dir}`

      files = Find.find(dir).reject { |x| File.directory? x }
      bump files.count

      files.each do |entry|
        sys.add(entry, dir)
        done
      end
    end

    add sys

    current 'Setting up Kibana'
    Kibana.create
  end

  def self.elasticsearch?
    @elasticsearch ||= { last: (Time.now - 75), status: 'red' }
    if (Time.now - @elasticsearch.last) > 60
      puts 'Checking Elasticsearch'
      @elasticsearch.last = Time.now
      @elasticsearch.status = HTTParty.get('http://elasticsearch:9200/_cluster/health').status
    end

    @elasticsearch.status
  rescue StandardError => e
    puts "Elasticsearch Offline? #{e.message}"
    false
  end
end
