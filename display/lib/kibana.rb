# Nasty JSON that doesn't seem to be a good way to go around it. It has to be
# set this way or kibana will 400 the request.

# Helper to create Kibana Saved Objects
module Kibana
  @index = nil
  @dashboard = nil

  def self.ready?
    System.elasticsearch? && @index
  end

  def self.clear
    @index = nil
    @dashboard = nil
  end

  def self.index
    @index
  end

  def self.dashboard
    @dashboard
  end

  def self.url
    'http://kibana:5601'
  end

  def self.headers
    { 'Content-Type' => 'application/json', 'kbn-xsrf' => 'anything' }
  end

  def self.create
    return false if @index

    puts 'Kibana: Index'
    payload = { attributes: { title: 'logstash-*', timeFieldName: 'time' } }
    resp = HTTParty.post(
      "#{url}/api/saved_objects/index-pattern",
      body: payload.to_json, headers: headers
    )

    @index = resp.id

    puts 'Kibana: Dashboard'
    HTTParty.post(
      "#{url}/api/kibana/settings/defaultIndex",
      body: { value: @index }.to_json, headers: headers
    )

    create_dashboard
    puts 'Kibana: Complete'
  end

  def self.create_dashboard
    resp = HTTParty.post(
      "#{url}/api/kibana/dashboards/import",
      body: dashboard_content.to_json,
      headers: headers
    )

    @dashboard = resp.objects.find { |x| x.type == 'dashboard' }.id
  end

  def self.query(body)
    HTTParty.post(
      'http://kibana:5601/elasticsearch/_msearch?rest_total_hits_as_int=true&ignore_throttled=true',
      body: body,
      headers: headers
    )
  end

  def self.pry
    # rubocop:disable Lint/Debugger
    binding.pry
    # rubocop:enable Lint/Debugger
  end

  def self.top_requests_by_duration
    return nil unless ready?

    resp = query File.read('kibana/top_requests_by_duration.json')
    buckets = resp.dig('responses', 0, 'aggregations', '4', 'buckets')

    return nil unless buckets

    buckets.map do |x|
      total = x['2']['value'] / 1000
      avg = x['3']['value']
      { path: x['key'], avg: avg, count: x['doc_count'], total: total }
    end
  end

  def self.sources
    return nil unless ready?

    resp = query File.read('kibana/sources_indexed.json')
    buckets = resp.dig('responses', 0, 'aggregations', '2', 'buckets')

    return nil unless buckets

    buckets
  end

  def self.export_dashboard
    dashboard_id = HTTParty.get(
      "#{kibana_url}/saved_objects/_find?type=dashboard&search=display",
      headers: headers
    ).dig('saved_objects', 0, 'id')

    resp = HTTParty.get(
      "#{kibana_url}/kibana/dashboards/export?dashboard=#{dashboard_id}",
      headers: headers
    )

    # rubocop:disable Lint/InterpolationCheck
    dash = resp.to_s.gsub(@index, '#{@index}')
    # rubocop:enable Lint/InterpolationCheck

    system("echo -n '#{dash}'| xclip -selection c")
  end

  def self.kibana_url
    'http://elasticsearch:5601/api'
  end
end
