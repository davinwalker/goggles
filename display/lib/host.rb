# Data formatter
class Host
  include InfoFormat
  include ActionView::Helpers::NumberHelper

  attr_accessor :id, :info, :archive, :include

  def initialize(archive, info = {})
    @id = SecureRandom.uuid
    @archive = archive.gsub('gitlabsos.', '') # .split('_', 2).first
    @info = info
  end

  def log_dir
    Sauce.settings.log_dir
  end

  def name
    if info.key? :hostname
      info.hostname.first
    else
      archive
    end
  end

  def filter
    id.delete('-')
  end

  def log_files
    Dir["#{log_dir}/#{name}/*"].sort
  end

  def category_list
    {
      cpuinfo: :cpu, lscpu: :cpu, etc_fstab: :disk, etc_os_release: :system,
      etc_security_limits_conf: :system, vmstat: :system, ntpq: :system,
      ps: :system, iostat: :disk, nfsstat: :disk, gitlab_status: :gitlab
    }
  end

  def category(field)
    if category_list.key? field
      category_list[field]
    else
      puts "Generic Field: #{field}"
    end
  end

  def status
    return nil unless info.key? :gitlab_status

    info.gitlab_status.map do |data|
      status, service = data.split(':')[0..1]
      color = if status == 'run'
                :green
              else
                :red
              end
      { status: color, name: service.lstrip }
    end.compact
  end

  def missing?(data)
    return false if data.nil?

    return true if data.join.downcase.include? 'no such file or directory'
    return true if data.join.downcase.include? ': command not found'

    false
  end

  def add(path, dir)
    thing = Thing.new(path, dir, name)

    # Store Current file being read
    System.current thing.name

    case thing.type
    when :raw, :info then info[thing.name.to_sym] = thing.raw
    when :log
      puts "Host: Not storing log: #{thing.name}"
    end
  end

  def percent(value, total)
    (value.to_f / total.to_f).round(2) * 100
  end

  def missing
    (info.keys - captured).reject { |x| missing?(info[x]) || info[x]&.empty? }
  end

  def captured
    %i[
      hostname disk_free free_m meminfo uname date uptime df_h netstat ps vmstat
    ]
  end

  def data?(key)
    return false unless info.key? key
    return false if info[key].nil?
    return false if info[key].empty?

    true
  end

  def icon
    release = info.keys.grep(/_release/).map { |x| info[x] }.join

    if release.include? 'ubuntu'
      'fa-ubuntu'
    elsif release.include? 'suse'
      'fa-suse'
    elsif release.include? 'redhat'
      'fa-redhat'
    elsif release.include? 'centos'
      'fa-centos'
    else
      'fa-linux'
    end
  end
end
