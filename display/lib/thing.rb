# File Manipulator
class Thing
  include InfoFormat

  attr_accessor :path, :dir, :size, :name, :raw, :host
  def initialize(path, dir, host)
    self.host = host
    self.path = path
    self.dir = dir
    self.name = build_path.gsub(/[^\w\s]/, '_')
    self.size = File.size(path) / (1024 * 1024)
    puts "Loading: #{name}:#{size}MB - #{type}"
    process
  end

  def type
    if info?
      :info
    elsif log?
      :log
    else
      :raw
    end
  end

  def info?
    methods.include? "format_#{name}".to_sym
  end

  def info
    self.raw = send "format_#{name}", read_raw
  end

  def log?
    # binding.pry
    # Limit any Log files
    # path.include?('log')

    if SuperLog.type? name
      true
    else
      false
    end
  end

  # No formatting, not a large log file
  def read_raw
    self.raw = File.read(path).split("\n")
  end

  # Read and Process
  def process
    case type
    when :info then info
    when :log then Log.new(self)
    else
      return false if size >= 5

      read_raw
    end
  end

  # Format the name of this thing
  def build_path(divider = '_')
    tmp_path = path.gsub(dir + '/', '')

    case tmp_path.count('/')
    when 0
      tmp_path
    when 1
      tmp_path.split('/').last
    else
      tmp_path.split('/').last(2).join(divider)
    end
  end
end
