# Log Manager
class Log
  include LogFormat

  attr_accessor :thing, :socket

  # Check to see if it should be loaded
  def initialize(thing)
    self.thing = thing

    if SuperLog.type? name
      puts "Log Parse: #{name}"
      process
    else
      puts "--- Not Logs: #{name}"
    end
  end

  # Send to ElK and add to log directory
  def process
    time = Benchmark.realtime { elk } if System.elasticsearch?

    puts "Processed #{name} in #{time.round(2)}"
    mv_file
  end

  # ---------------------------
  # Quick Access
  def path
    thing.path
  end

  def name
    thing.name
  end

  def host
    thing.host
  end

  def build_path(args = nil)
    thing.build_path(args)
  end

  def log_dir
    Sauce.settings.log_dir
  end
  # ---------------------------

  # Store File for Later access
  def mv_file
    Dir.mkdir("#{log_dir}/#{host}") unless Dir.exist? "#{log_dir}/#{host}"

    puts "Move: #{path}"
    FileUtils.mv(path, "#{log_dir}/#{host}/#{mv_format}")
  end

  def mv_format
    path.split('gitlab/', 2).last.tr('/', '_')
  end

  def mia
    puts 'Log: MIA:' + name
  end

  # Ship the Logs to ELK
  def elk
    if SuperLog.type?(name)
      send(SuperLog.type?(name))
    else
      puts "Log Format missing: #{name}"
      nil
    end
    socket_close
  end

  def post(row)
    loop do
      self.socket = TCPSocket.new('logstash', 5000) if socket.nil?
      socket.puts Oj.dump(row)
      break
    rescue StandardError => e
      puts "Logtash - #{e.message}"
      # Wait and Retry
      sleep 1
    end
  end

  def socket_close
    return true unless socket

    socket.close
    self.socket = nil
  end
end
