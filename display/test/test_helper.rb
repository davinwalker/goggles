$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

ENV['TEST'] = 'true'

require 'pry'
require 'minitest/reporters'
require 'minitest/autorun'
require 'rack/test'

Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new(color: true)]

require File.expand_path '../app.rb', __dir__
