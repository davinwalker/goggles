require File.expand_path 'test_helper.rb', __dir__

# Basic Test to make sure the app loads
class DoTheTest < Minitest::Test
  include Rack::Test::Methods

  def app
    Sauce
  end

  def test_root
    get '/'
    assert last_response.ok?
  end
end
