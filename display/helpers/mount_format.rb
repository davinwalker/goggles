# data = File.read('example_mounts.txt').split("\n")

# Helper with Formatting
module MountFormat
  def self.parse(data)
    data.map do |row|
      next unless row.include? 'nfs'

      source, mnt, type, details, numbers = row.split(' ', 5)
      next unless type.include? 'nfs'

      {
        source: source, mnt: mnt, type: type, numbers: numbers,
        params: param_parse(details)
      }
    end.compact
  end

  def self.param_parse(details)
    params = details.split('rw,relatime,').last

    params = params.split(',').each_with_object({}) do |v, obj|
      if v.include? '='
        key, value = v.split('=')
      else
        key = v
        value = true
      end
      obj[key] = value
      obj
    end

    params.sort_by { |k, _v| k }.to_h
  end
end
