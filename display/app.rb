#!/usr/bin/env ruby

Bundler.require(:default)
require 'find'
Hash.use_dot_syntax = true

# Docker Sync
STDOUT.sync = true

# Fix symbol keys
Oj.default_options = { mode: :compat }

require_all 'helpers'
require_all 'lib'

# Modular App to Wrap for Temp Directory
class Sauce < Sinatra::Base
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper
  enable :sessions
  set :bind, '0.0.0.0'

  def log_dir
    settings.log_dir
  end

  get '/' do
    session[:current] = nil
    slim :index
  end

  get '/graphs' do
    slim :graphs
  end

  get '/logs' do
    slim :logs
  end

  # Easy Reset of Instance
  get '/delete' do
    HTTParty.delete('http://elasticsearch:9200/*') if System.elasticsearch?
    System.clear
    Kibana.clear
    redirect '/'
  end

  get '/loading' do
    unless System.loading.zero?
      while session[:current] == System.current
        puts "#{session[:current]} == #{session[:current]}"
        sleep 0.5
        break if session[:current].nil?

      end
      session[:current] = System.current
    end

    redirect '/' if System.loading.zero?
    slim :loading
  end

  post '/upload' do
    # System.upload(params[:file])
    Thread.new { System.upload(params[:file]) } if params[:file]

    redirect '/loading'
  end

  get '/log/file' do
    if File.exist? params['file']
      slim :log, locals: { data: File.read(params['file']) }
    else
      redirect '/'
    end
  end

  # rubocop:disable Lint/Debugger
  get '/pry' do
    host = System.list.first
    info = host.info
    binding.pry unless ENV['PRY']
    redirect '/'
  end
  # rubocop:enable Lint/Debugger

  # Crash Redirect
  get '/upload' do
    redirect '/'
  end
end

Dir.mktmpdir('sauce-') do |log_dir|
  Sauce.set :log_dir, log_dir
  Sauce.run! unless ENV['TEST']
end
